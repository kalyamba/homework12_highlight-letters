const buttons = document.querySelectorAll('.btn');

function handleKeyDown(event) {
    const key = event.key.toUpperCase();

    buttons.forEach(button => {
        if (button.innerText === key) {
            buttons.forEach(btn => btn.classList.remove('active'));

            button.classList.add('active');
        } else if (button.classList.contains('active')) {
            button.classList.remove('active');
        }
    });
}

document.addEventListener('keydown', handleKeyDown);

document.querySelector('.btn-wrapper').addEventListener('submit', function (event) {
    event.preventDefault();
});
